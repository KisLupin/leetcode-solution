import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;
import java.util.stream.IntStream;

public class Main {

    public static void main(String[] args) {
//        ListNode root = new ListNode(1, new ListNode());
//        ListNode node2 = new ListNode(2, new ListNode());
//        ListNode node3 = new ListNode(3, new ListNode());
//        ListNode node4 = new ListNode(4, new ListNode());
//        ListNode node5 = new ListNode(5, new ListNode());
//        root.next = node2;
//        node2.next = node3;
//        node3.next = node4;
//        node4.next = node5;
//        node5.next = new ListNode(6, null);
//
//        ListNode listNode = reverseKGroup(root, 3);
//        int[][] image = new int[][]{
//                {1, 2, 4},
//                {6, 5, 7},
//                {9, 6, 8}
//        };
//        maxAreaOfIsland(image);
//        int[] nums1 = new int[]{4, 9, 5}, nums2 = new int[]{7, 2, 5, 1, 3, 4};
        int[][] matrix = new int[][]{
                {1, 2, 3}, {4, 5, 6}, {7, 8, 9}
        };
        System.out.println(Arrays.deepToString(matrixBlockSum(matrix, 2)));
    }

    public static int[][] matrixBlockSum(int[][] mat, int K) {
        int n = mat.length;
        int m = mat[0].length;
        int[][] temp = new int[n + 1][m + 1];
        for (int i = 0; i < n; i++) {
            temp[i + 1][1] = temp[i][1] + mat[i][0];
        }
        for (int i = 0; i < m; i++) {
            temp[1][i + 1] = temp[1][i] + mat[0][i];
        }
        for (int i = 1; i < n; i++) {
            for (int j = 1; j < m; j++) {
                temp[i + 1][j + 1] = temp[i][j + 1] + temp[i + 1][j] - temp[i][j] + mat[i][j];
            }
        }
        int[][] res = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                int x1 = Math.max(i - K, 0);
                int y1 = Math.max(j - K, 0);
                int x2 = Math.min(i + K + 1, n);
                int y2 = Math.min(j + K + 1, m);
                res[i][j] = temp[x2][y2] - temp[x2][y1] - temp[x1][y2] + temp[x1][y1];
            }
        }
        return res;
    }

    public static int uniquePathsWithObstacles(int[][] arr) {
        if (arr == null || arr.length == 0)
            return 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i][j] ^= 1;
                if (arr[i][j] == 1) {
                    if (i == 0 && j != 0)
                        arr[i][j] = arr[i][j - 1];
                    if (i != 0 && j == 0)
                        arr[i][j] = arr[i - 1][j];
                    if (i != 0 && j != 0)
                        arr[i][j] = arr[i - 1][j] + arr[i][j - 1];
                }
            }
        }
        return arr[arr.length - 1][arr[0].length - 1];
    }

    public static int uniquePaths(int m, int n) {
        int[][] dp = new int[m][n];

        for (int i = 0; i < n; i++) {
            dp[0][i] = 1;
        }
        for (int i = 0; i < m; i++) {
            dp[i][0] = 1;
        }

        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }

        return dp[m - 1][n - 1];
    }

    public static int findMin(int[] nums) {
        if (nums == null || nums.length == 0) return -1;
        if (nums[nums.length - 1] > nums[0]) {
            return nums[0];
        }

        int lo = 0;
        int hi = nums.length - 1;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if (mid != 0 && nums[mid - 1] > nums[mid]) {
                return nums[mid];
            }
            if (nums[mid] >= nums[0]) {
                lo = mid + 1;
            } else {
                hi = mid - 1;
            }
        }
        //only get here when there is only one element
        return nums[hi];
    }

    public static int search(int[] nums, int target) {
        int left = 0;
        int right = nums.length - 1;
        int mid;
        while (left <= right) {
            mid = left + (right - left) / 2;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] <= nums[right]) {
                if (nums[mid] <= target && nums[right] >= target) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            } else {
                if (nums[mid] >= target && nums[left] <= target) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }
        }
        return -1;
    }

    public static int[] searchRange(int[] nums, int target) {
        return new int[]{
                binarySearch(nums, target, true),
                binarySearch(nums, target, false)
        };
    }

    public static int binarySearch(int[] nums, int target, boolean isLeft) {
        int low = 0;
        int high = nums.length - 1;
        int index = -1;
        while (low <= high) {
            int mid = (low + high) / 2;

            if (nums[mid] < target) {
                low = mid + 1;
            } else if (nums[mid] > target) {
                high = mid - 1;
            } else {
                index = mid;
                if (isLeft) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            }
        }
        return index;
    }

    public static TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        int min = Math.min(p.val, q.val);
        int max = Math.max(p.val, q.val);

        while (true) {
            if (root.val < min) {
                root = root.right;
            } else if (root.val > max) {
                root = root.left;
            } else {
                return root;
            }
        }
    }

    static Set<Integer> collection = new HashSet<>();

    public static boolean findTarget(TreeNode root, int k) {
        if (root == null) {
            return false;
        }
        if (collection.contains(k - root.val)) {
            return true;
        }
        collection.add(root.val);
        return (findTarget(root.left, k) || findTarget(root.right, k));
    }

    public static boolean isValidBST(TreeNode root) {
        return helper(root, null, null);
    }

    public static boolean helper(TreeNode root, Integer min, Integer max) {
        if (root == null) {
            return true;
        }

        if ((min != null && root.val <= min) || (max != null && root.val >= max)) {
            return false;
        }

        return helper(root.left, min, root.val) && helper(root.right, root.val, max);
    }

    public static int minimumTotal(List<List<Integer>> triangle) {
        int n = triangle.size();

        // initialize arr with the last row
        int[] arr = new int[n];
        for (int i = 0; i < n; i++)
            arr[i] = triangle.get(n - 1).get(i);

        // bottom up compare
        for (int i = n - 2; i >= 0; i--)
            for (int j = 0; j <= i; j++)
                arr[j] = Math.min(arr[j], arr[j + 1]) + triangle.get(i).get(j);

        return arr[0];
    }

    public static int minFallingPathSum(int[][] A) {
        if (A.length == 0 || A[0].length == 0) {
            return 0;
        }
        int rowLength = A.length;
        int colLength = A[0].length;
        for (int row = 1; row < rowLength; row++) {
            for (int col = 0; col < colLength; col++) {
                int curSum = Integer.MAX_VALUE;
                for (int k = -1; k <= 1; k++) {
                    int colTemp = col + k;
                    if (colTemp >= 0 && colTemp <= colLength - 1) {
                        curSum = Math.min(curSum, A[row - 1][colTemp] + A[row][col]);
                    }
                }
                A[row][col] = curSum;
            }
        }

        int min = Integer.MAX_VALUE;
        for (int n : A[A.length - 1]) {
            min = Math.min(min, n);
        }
        return min;
    }

    public static int numberOfArithmeticSlices(int[] nums) {
        int len = nums.length;
        if (len < 3) return 0;
        int[] dp = new int[len];
        int ans = 0;

        for (int i = 2; i < len; i++) {
            if (nums[i] - nums[i - 1] == nums[i - 1] - nums[i - 2]) {
                dp[i] = dp[i - 1] + 1;
                ans += dp[i];
            }
        }
        return ans;
    }

    public static int numTrees(int n) {
        int[] dp = new int[n + 1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 0; j < i; j++) {
                dp[i] += dp[j] * dp[i - j - 1];
            }
        }
        return dp[n];
    }

    public static int nthUglyNumber(int n) {
        int[] ugly = new int[n];
        ugly[0] = 1;
        int index2 = 0, index3 = 0, index5 = 0;
        int factor2 = 2, factor3 = 3, factor5 = 5;
        for (int i = 1; i < n; i++) {
            int min = Math.min(Math.min(factor2, factor3), factor5);
            ugly[i] = min;
            if (factor2 == min) {
                factor2 = 2 * ugly[++index2];
            }
            if (factor3 == min) {
                factor3 = 3 * ugly[++index3];
            }
            if (factor5 == min) {
                factor5 = 5 * ugly[++index5];
            }
        }
        return ugly[n - 1];
    }

    public static int numDecoding(String s) {
        if (s.isEmpty())
            return 0;

        int prev = 26;
        int result = 0;
        int first = 1, second = 1;

        for (int i = s.length() - 1; i >= 0; i--) {
            int digit = s.charAt(i) - '0';
            if (digit == 0) {
                result = 0;
            } else {
                result = first + (digit * 10 + prev <= 26 ? second : 0);
            }
            second = first;
            first = result;
            prev = digit;
        }

        return result;
    }

    public static int trap(int[] height) {
        if (height.length == 0) {
            return 0;
        }

        int maxht = 0, index = 0;
        for (int i = 0; i < height.length; i++) {
            if (height[i] > maxht) {
                maxht = height[i];
                index = i;
            }
        }

        int localmax = height[0];
        int water = 0;
        for (int i = 0; i < index; i++) {
            if (height[i] < localmax) {
                water += localmax - height[i];
            }
            localmax = Math.max(localmax, height[i]);
        }

        localmax = height[height.length - 1];
        for (int i = height.length - 1; i > index; i--) {
            if (height[i] < localmax) {
                water += localmax - height[i];
            }
            localmax = Math.max(localmax, height[i]);
        }

        return water;
    }

    public static boolean wordBreak(String s, List<String> wordDict) {
        boolean[] dp = new boolean[s.length() + 1];
        int minLen = Integer.MAX_VALUE;
        int maxLen = Integer.MIN_VALUE;
        for (String word : wordDict) {
            minLen = Math.min(minLen, word.length());
            maxLen = Math.max(maxLen, word.length());
        }

        dp[0] = true;
        for (int i = minLen; i <= s.length(); i++) {
            int j = Math.max(i - maxLen, 0);
            for (; j <= i - minLen; j++) {
                if (dp[j] && wordDict.contains(s.substring(j, i))) {
                    dp[i] = true;
                    break;
                }
            }
        }

        return dp[s.length()];
    }

    public static TreeNode insertIntoBST(TreeNode root, int val) {
        if (root == null) {
            return new TreeNode(val);
        }
        TreeNode node = root;
        while (node.val != val) {
            if (node.val > val && node.left == null) {
                node.left = new TreeNode(val);
            } else if (node.val < val && node.right == null) {
                node.right = new TreeNode(val);
            }
            node = node.val > val ? node.left : node.right;
        }
        return root;
    }

    public static TreeNode searchBST(TreeNode root, int val) {
        if (root != null) {
            Deque<TreeNode> searchStack = new ArrayDeque<>();
            searchStack.offerFirst(root);
            while (searchStack.peekFirst() != null) {
                TreeNode searchNode = searchStack.pollFirst();
                if (searchNode.val == val) {
                    return searchNode;
                } else if (searchNode.val < val) {
                    if (searchNode.right != null) {
                        searchStack.offerFirst(searchNode.right);
                    }
                } else {
                    if (searchNode.left != null) {
                        searchStack.offerFirst(searchNode.left);
                    }
                }
            }
        }
        return null;
    }

    public static boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) return false;
        if (root.left == null && root.right == null && root.val == sum) return true;
        int restSum = sum - root.val;
        return hasPathSum(root.left, restSum) || hasPathSum(root.right, restSum);
    }

    public static TreeNode invertTree(TreeNode root) {
        if (root == null) return root;
        LinkedList<TreeNode> q = new LinkedList<>();
        q.add(root);
        while (!q.isEmpty()) {
            TreeNode n = q.peekFirst();
            q.removeFirst();
            if (n.left != null) q.add(n.left);
            if (n.right != null) q.add(n.right);
            TreeNode buf = n.left;
            n.left = n.right;
            n.right = buf;
        }
        return root;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public static boolean canJump(int[] nums) {
        int last = nums.length - 1; // last index/ position to reach
        for (int i = nums.length - 2; i >= 0; i--) {
            if (i + nums[i] >= last) { // check if i+nums[i] >= last means we can reach to that index
                //with curr value and from current index and if yes then
                //change last to current index
                last = i;
            }
        }
        return last <= 0;
    }

    public static int orangesRotting(int[][] grid) {
        int rowSize = grid.length, colSize = grid[0].length, minutes = 0, fresh = 0;
        Queue<int[]> queue = new LinkedList();

        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < colSize; j++) {
                if (grid[i][j] == 2) {
                    queue.add(new int[]{i, j});
                    grid[i][j] = 0;
                } else if (grid[i][j] == 1)
                    fresh++;
            }
        }

        while (!queue.isEmpty()) {
            int size = queue.size();
            while (size-- > 0) {
                int[] tm = queue.poll(); //get first orange is rotted
                int row = tm[0]; //row
                int col = tm[1]; //column
                // check 4-directionally adjacent if [][] == 1 add to queue
                if (row + 1 < rowSize && grid[row + 1][col] == 1) {
                    queue.add(new int[]{row + 1, col});
                    grid[row + 1][col] = 0;
                    fresh--;
                }
                if (col + 1 < colSize && grid[row][col + 1] == 1) {
                    queue.add(new int[]{row, col + 1});
                    grid[row][col + 1] = 0;
                    fresh--;
                }
                if (row - 1 >= 0 && grid[row - 1][col] == 1) {
                    queue.add(new int[]{row - 1, col});
                    grid[row - 1][col] = 0;
                    fresh--;
                }
                if (col - 1 >= 0 && grid[row][col - 1] == 1) {
                    queue.add(new int[]{row, col - 1});
                    grid[row][col - 1] = 0;
                    fresh--;
                }
            }

            if (queue.size() > 0) {
                minutes++;
            }
        }

        if (fresh > 0)
            return -1;
        return minutes;
    }

    public static int[][] updateMatrix(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return matrix;
        }

        int rows = matrix.length;
        int cols = matrix[0].length;
        if (rows == 1 && cols == 1) {
            return matrix;
        }

        int[][] result = new int[rows][cols];
        // (rows + cols - 1) is the maximum possible distance in the matrix.
        // It's the distance been two diagonally opposite corners.
        int maxDistance = rows + cols;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                if (matrix[i][j] == 0) {
                    continue;
                }
                result[i][j] = maxDistance;
                if (i > 0) {
                    result[i][j] = Math.min(result[i][j], result[i - 1][j] + 1);
                }
                if (j > 0) {
                    result[i][j] = Math.min(result[i][j], result[i][j - 1] + 1);
                }
            }
        }

        for (int i = rows - 1; i >= 0; i--) {
            for (int j = cols - 1; j >= 0; j--) {
                if (matrix[i][j] == 0) {
                    continue;
                }
                if (i < rows - 1) {
                    result[i][j] = Math.min(result[i][j], result[i + 1][j] + 1);
                }
                if (j < cols - 1) {
                    result[i][j] = Math.min(result[i][j], result[i][j + 1] + 1);
                }
            }
        }

        return result;
    }

    public static int maxProfit(int[] prices) {
        int maxProfit = 0;
        int min = prices[0];
        for (int i = 1; i < prices.length; i++) {
            if (prices[i] == Math.min(min, prices[i])) { //save min when buy
                min = prices[i];
            } else {                                    //calculate max sold and save to max
                maxProfit = Math.max(maxProfit, prices[i] - min);
            }
        }
        return maxProfit;
    }

    public static int[] intersect(int[] nums1, int[] nums2) {
        List<Integer> a = new ArrayList();
        int[] temp = new int[1001];
        for (int k : nums1) {
            temp[k]++;
        }
        for (int k : nums2) {
            if (temp[k] > 0) {
                a.add(k);
                temp[k]--;
            }
        }
        int[] ar = new int[a.size()];
        for (int i = 0; i < ar.length; i++) {
            ar[i] = a.get(i);
        }
        return ar;
    }

    public static List<Integer> findSubstring(String s, String[] words) {
        List<Integer> res = new ArrayList<>();
        Map<String, Integer> map = new HashMap<>();
        boolean[] visited = new boolean[s.length()];
        int m = words.length, n = words[0].length();
        int len = m * n, lastLeft = s.length() - len, lastRight = s.length() - n;
        // store all words with their count
        for (String word : words) {
            map.put(word, map.getOrDefault(word, 0) + 1);
        }
        for (int i = 0; i <= lastLeft; i++) { // start point of each search
            if (visited[i]) {
                continue;
            }
            visited[i] = true; // mark the start point
            int total = 0, left = i, right = i;
            Map<String, Integer> matchMap = new HashMap<>();
            while (right <= lastRight) {
                visited[left] = true; // mark the start point
                String str = s.substring(right, right + n); // get new substring
                if (!map.containsKey(str)) { // check if the substring exists in dictionary
                    break;
                }
                int cnt = matchMap.getOrDefault(str, 0); // check if it matches the count in dictionary
                if (cnt < map.get(str)) { // if less, add the new substring to match map
                    total++;
                    matchMap.put(str, cnt + 1);
                    right += n;   // move the right of the window
                    if (total == m) { // if totally matched, add the start point to the result list
                        res.add(left);
                    }
                } else {  // if over, remove the left most substring, also change the start point (left)
                    total--;
                    str = s.substring(left, left + n);  // note: the str changes
                    matchMap.put(str, matchMap.get(str) - 1);
                    left += n;  // move the left of the window
                }
            }
        }
        return res;
    }

    public static int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                return new int[]{i, map.get(nums[i])};
            } else {
                map.put(target - nums[i], i);
            }
        }
        return new int[2];
    }

    public static int deleteAndEarn(int[] nums) {
        // Get the max elt
        int m = IntStream.of(nums).max().getAsInt();
        int n = nums.length;
        if (n == 1) return nums[0];
        // Create a s for values s with default 0 | and a vector for DP
        int[] s = new int[m + 1];
        int[] dp = new int[m + 1];
        // Adjust s content
        for (int num : nums) {
            s[num]++;
        }
        dp[0] = 0;
        dp[1] = s[1];
        for (int i = 2; i <= m; i++) {
            // Max of taking the current value x times Vs keeping the value before (Since we're removing "n-1" & "n+1")
            dp[i] = Math.max(dp[i - 1], dp[i - 2] + s[i] * i);
        }
        return dp[m];
    }

    public static int rob(int[] nums) {
        if (nums.length == 0) return 0;
        if (nums.length == 1) return nums[0];
        int[] nums1 = new int[nums.length];
        int[] nums2 = new int[nums.length];
        System.arraycopy(nums, 0, nums1, 0, nums.length - 1);
        System.arraycopy(nums, 1, nums2, 0, nums.length - 1);
        return Math.max(robSimple(nums1), robSimple(nums2));
    }

    public static int robSimple(int[] nums) {
        int a = 0; // house robbed
        int b = 0; // house not robbed
        for (int num : nums) {
            int curr = b + num; // not robbed earlier, so rob curr one i.e. i th one, as let i-1 is not robbed
            b = Math.max(a, b); // max of robed and not robbed house
            a = curr; // robbed curr house
        }
        return Math.max(a, b);
    }

    public static Node connect(Node root) {
        if (root == null)
            return null;
        if (root.left != null)
            recc(root.left, root.right);
        return root;
    }

    public static void recc(Node l, Node r) {
        l.next = r;
        if (l.right != null) {
            if (l.right.next == null)
                l.right.next = l.next.left;
            if (r.right.next == null && r.next != null)
                r.right.next = r.next.left;
            recc(l.left, l.right);
            recc(r.left, r.right);
        }
    }

    static class Node {
        public int val;
        public Node left;
        public Node right;
        public Node next;

        public Node() {
        }

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, Node _left, Node _right, Node _next) {
            val = _val;
            left = _left;
            right = _right;
            next = _next;
        }
    }

    public static int firstMissingPositive(int[] nums) {
        int n = nums.length;
        int[] memo = new int[n];
        // store occurrence for nums within [1, n]
        for (int num : nums) {
            if (num > 0 && num <= n) {
                memo[num - 1]++;
            }
        }
        // check missing positive
        for (int i = 0; i < n; i++) {
            if (memo[i] == 0) return i + 1;
        }
        return n + 1;
    }

    public static int minCostClimbingStairs(int[] cost) {
        for (int i = 2; i < cost.length; i++) {
            cost[i] += Math.min(cost[i - 1], cost[i - 2]);
        }
        return Math.min(cost[cost.length - 1], cost[cost.length - 2]);
    }

    public static int climbStairs(int n) {
        if (n < 3) {
            return n;
        }
        int[] dp = new int[n + 1];
        dp[1] = 1;
        dp[2] = 2;
        dp[3] = 3;
        for (int i = 3; i < n + 1; i++) {
            dp[i] = dp[i - 1] + dp[i - 2];
        }
        return dp[n];
    }

    public static int triBib(int n) {
        if (n == 0) return 0;
        if (n < 3) return 1;
        if (n < 4) return 2;
        int fib0 = 2;
        int fib1 = 1;
        int fib2 = 1;
        for (int i = 3; i < n; i++) {
            int temp = fib0;
            fib0 = fib0 + fib1 + fib2;
            fib2 = fib1;
            fib1 = temp;
        }
        return fib0;
    }

    public static int maxSubArray(int[] nums) {
        int max = nums[0];
        int cur = 0;
        for (int n : nums) {
            if (cur < 0) cur = 0;
            cur += n;
            max = Math.max(cur, max);
        }
        return max;
    }

    public static boolean[][] traversed;

    public static int maxAreaOfIsland(int[][] grid) {
        traversed = new boolean[grid.length][grid[0].length];
        int result = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 1 && (!traversed[i][j])) {
                    result = Math.max(dfs(grid, i, j), result);
                }
            }
        }
        return result;
    }

    public static int dfs(int[][] grid, int row, int col) {
        if (row >= grid.length || col >= grid[0].length || row < 0 || col < 0)
            return 0;
        if (grid[row][col] == 0 || traversed[row][col])
            return 0;
        else {
            traversed[row][col] = true;
            int t1 = dfs(grid, row + 1, col);
            int t2 = dfs(grid, row - 1, col);
            int t3 = dfs(grid, row, col + 1);
            int t4 = dfs(grid, row, col - 1);
            return t1 + t2 + t3 + t4 + 1;
        }
    }

    public static int[][] floodFillRecursive(int[][] image, int row, int col, int newColor) {
        int original = image[row][col];
        return floodFillHelp(image, row, col, newColor, original);
    }

    public static int[][] floodFillHelp(int[][] image, int row, int col, int newColor, int original) {
        int rowSize = image.length;
        int colSize = image[0].length;
        if (row < 0 || row >= rowSize || col < 0 || col >= colSize) {
            return image;
        }
        if (original == newColor) {
            return image;
        }

        image[row][col] = newColor;

        if (col + 1 < colSize && image[row][col + 1] == original)
            floodFillHelp(image, row, col + 1, newColor, original);
        if (col - 1 >= 0 && image[row][col - 1] == original) floodFillHelp(image, row, col - 1, newColor, original);
        if (row - 1 >= 0 && image[row - 1][col] == original) floodFillHelp(image, row - 1, col, newColor, original);
        if (row + 1 < rowSize && image[row + 1][col] == original)
            floodFillHelp(image, row + 1, col, newColor, original);

        return image;
    }

    public static int[][] floodFill(int[][] image, int sr, int sc, int newColor) {
        int[][] v = new int[image.length][];
        for (int i = 0; i < image.length; ++i) v[i] = new int[image[0].length];
        Queue<int[]> q = new LinkedList<>();
        q.offer(new int[]{sr, sc});
        int curr = image[sr][sc];
        int[][] dir = new int[][]{{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        while (!q.isEmpty()) {
            int sz = q.size();
            for (int i = 0; i < sz; ++i) {
                int[] f = q.poll();
                v[f[0]][f[1]] = 1;
                image[f[0]][f[1]] = newColor;
                for (int[] d : dir) {
                    int r = f[0] + d[0], c = f[1] + d[1];
                    if (r >= image.length || r < 0 || c >= image[0].length || c < 0 || image[r][c] != curr) {
                        continue;
                    }
                    if (v[r][c] == 0) q.offer(new int[]{r, c});
                }
            }
        }
        return image;
    }

    public static boolean checkInclusion(String s1, String s2) {
        if (s1.length() > s2.length()) return false;

        int[] s1count = new int[26];
        int[] s2count = new int[26];

        for (int i = 0; i < s1.length(); i++) {
            s1count[s1.charAt(i) - 'a']++;
            s2count[s2.charAt(i) - 'a']++;
        }

        int slow = 0;
        int fast = s1.length() - 1;

        while (fast < s2.length()) {
            if (Arrays.equals(s1count, s2count)) {
                return true;
            }
            int pos = s2.charAt(slow++) - 'a';
            s2count[pos]--;

            if (++fast < s2.length())
                s2count[s2.charAt(fast) - 'a']++;
        }
        return false;
    }

    public static void printDistinctPermutn(String str, String ans, List<String> list) {
        // If string is empty
        if (str.length() == 0) {
            // print ans
            list.add(ans);
            return;
        }
        // Make a boolean array of size '26' which
        // stores false by default and make true
        // at the position which alphabet is being
        // used
        boolean[] alpha = new boolean[26];
        for (int i = 0; i < str.length(); i++) {
            // ith character of str
            char ch = str.charAt(i);
            // Rest of the string after excluding
            // the ith character
            String ros = str.substring(0, i) +
                    str.substring(i + 1);
            // If the character has not been used
            // then recursive call will take place.
            // Otherwise, there will be no recursive
            // call
            if (!alpha[ch - 'a'])
                printDistinctPermutn(ros, ans + ch, list);
            alpha[ch - 'a'] = true;
        }
    }

    public static int lengthOfLongestSubstring(String s) {
        int start = 0, max = 0, r = 0;
        int[] a = new int[256];
        for (int end = 0; end < s.length(); end++) {
            a[s.charAt(end)]++;
            if (a[s.charAt(end)] == 2) {
                r = 1;
            }
            while (r == 1) {
                if (a[s.charAt(start)] == 2) {
                    r = 0;
                }
                a[s.charAt(start++)]--;
            }
            max = Math.max(max, end - start + 1);
        }
        return max;
    }

    public static ListNode removeNthFromEnd(ListNode head, int n) {
        if (head == null) {
            return null;
        }
        ListNode slow = head, fast = head;

        for (int i = 1; i <= n; i++) {
            fast = fast.next;
        }

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next;
        }

        // if there is a single element and we want to delete that
        if (slow == head && fast == null) {
            head = head.next;
            return head;
        }

        if (slow != null && slow.next != null) {
            slow.next = slow.next.next;
        }
        return head;
    }

    public static String reverseWords(String s) {
        String[] arr = s.split(" ");
        String result = "";
        for (String st : arr) {
            String res = reverseString(st.toCharArray());
            result += res + " ";
        }
        return result.trim();
    }

    public static String reverseString(char[] s) {
        for (int i = 0; i < s.length / 2; i++) {
            swap(s, i, s.length - i - 1);
        }
        return String.valueOf(s);
    }

    public static void swap(char[] s, int pos1, int pos2) {
        char temp = s[pos1];
        s[pos1] = s[pos2];
        s[pos2] = temp;
    }

    public static int[] twoSumOrdered(int[] numbers, int target) {
        int left = 0;
        int end = numbers.length - 1;
        int[] res = new int[2];
        while (left < end) {
            if (numbers[end] + numbers[left] > target) {
                end--;
                continue;
            }
            if (numbers[end] + numbers[left] < target) {
                left++;
                continue;
            }
            if (numbers[end] + numbers[left] == target) {
                res[0] = left + 1;
                res[1] = end + 1;
                break;
            }
        }
        return res;
    }

    public static void moveZeroes(int[] nums) {
        int left = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != 0) {
                nums[left] = nums[i];
                left++;
            }
        }

        while (left < nums.length) {
            nums[left] = 0;
            left++;
        }
    }

    public static void rotate(int[] nums, int k) {
        k = k % nums.length;
        int[] a = new int[nums.length - k];
        int[] b = new int[k];
        if (nums.length - k >= 0) {
            System.arraycopy(nums, 0, a, 0, nums.length - k);
        }
        if (nums.length - (nums.length - k) >= 0) {
            System.arraycopy(nums, nums.length - k, b, 0, nums.length - (nums.length - k));
        }
        System.arraycopy(a, 0, nums, k, nums.length - k);
        System.arraycopy(b, 0, nums, 0, k);
    }

    public static int[] sortedSquares(int[] nums) {
        int start = 0;
        int end = nums.length - 1;
        int index = end;
        int[] res = new int[end + 1];
        while (index >= 0) {
            if (Math.abs(nums[start]) > Math.abs(nums[end])) {
                res[index] = (int) Math.pow(nums[start], 2);
                start++;
            } else {
                res[index] = (int) Math.pow(nums[end], 2);
                end--;
            }
            index--;
        }
        return res;
    }

    public static int searchInsert(int[] nums, int target) {
        if (nums[0] >= target) {
            return 0;
        }
        int length = nums.length;
        int posMid = length / 2;
        if (nums[length - 1] < target) {
            return length;
        }
        if (nums[length - 1] == target) {
            return length - 1;
        }
        int mid = nums[length / 2];
        if (target == mid) {
            return length / 2;
        }
        if (target < mid) {
            for (int i = posMid; i > 0; i--) {
                if (nums[i] > target && target > nums[i - 1]) {
                    return i;
                }
                if (nums[i] == target) {
                    return i;
                }
            }
        }
        if (target > mid) {
            for (int i = posMid; i < length - 1; i++) {
                if (target == nums[i]) {
                    return i;
                }
                if (target > nums[i] && target < nums[i + 1]) {
                    return i + 1;
                }
            }
        }
        return 0;
    }

    public static int longestValidParentheses(String s) {
        if (s.length() < 2) {
            return 0;
        }
        Stack<Integer> st = new Stack<>();
        st.push(-1);
        st.push(0);
        int[] dp = new int[s.length()];

        for (int pos = 1; pos < s.length(); pos++) {
            if (s.charAt(pos) == '(') {
                st.push(pos);
                dp[pos] = dp[pos - 1];
            } else {                    // ')'
                if (st.peek() == -1) { // st is empty
                    dp[pos] = dp[pos - 1];
                    st.push(pos);
                } else if (s.charAt(st.peek()) == '(') {
                    st.pop();
                    dp[pos] = Math.max(pos - st.peek(), dp[pos - 1]);
                } else {
                    st.push(pos);
                    dp[pos] = dp[pos - 1];
                }
            }
        }
        return dp[s.length() - 1];
    }

    public static int length(ListNode head) {
        int len = 0;
        while (head != null) {
            len++;
            head = head.next;
        }
        return len;
    }

    public static ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode reversed = reverseList(head.next);
        head.next.next = head;
        head.next = null;
        return reversed;
    }

    public static ListNode reverseKGroup(ListNode head, int k) {
        int len = length(head);
        if (k == 1 || len == 1) return head;
        ListNode dummy = new ListNode(0);
        ListNode res = dummy;

        ListNode firstInGroup = head;
        while (firstInGroup != null) {
            ListNode lastInGroup = firstInGroup;
            for (int i = 0; i < k - 1 && lastInGroup != null; i++) {
                lastInGroup = lastInGroup.next;
            }
            if (lastInGroup == null) {
                dummy.next = firstInGroup;
                break;
            }
            ListNode nextInGroup = lastInGroup.next;
            lastInGroup.next = null;
            dummy.next = reverseList(firstInGroup);
            dummy = firstInGroup;
            firstInGroup = nextInGroup;
        }
        return res.next;
    }

    public static ListNode mergeKLists(ListNode[] lists) {
        List<Integer> list = new ArrayList<>();
        for (ListNode node : lists) {
            if (node == null) {
                continue;
            }
            while (node.next != null) {
                list.add(node.val);
                node = node.next;
            }
            list.add(node.val);
        }
        if (list.isEmpty()) {
            return null;
        }
        Collections.sort(list);
        ListNode root = new ListNode(0);
        ListNode tail = root;
        for (int i = 0; i < list.size(); i++) {
            if (i < list.size() - 1) {
                tail.next = new ListNode(list.get(i), new ListNode());
                tail = tail.next;
            }
            tail.next = new ListNode(list.get(i), null);
        }
        return root.next.next;
    }

    public static class ListNode {
        int val;
        ListNode next;

        ListNode() {
        }

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }

    public static int minEatingSpeed(int[] piles, int h) {
        int i, low = 1, mid, high = Integer.MIN_VALUE, n = piles.length;

        for (i = 0; i < n; i++) {
            high = Math.max(high, piles[i]);
        }

        // the max speed = size of largest pile
        // Why ?
        // say max is 20 bananas in a pile
        // even if Koko eats at the speed of 100 bananas per hour, she will finish off 20 bananas in 12 minutes
        // and will have to idle around for next 48 minutes - so speed exceeding max value is meaningless

        // find the lowest possible speed to meet the condition
        while (low < high) {
            mid = low + ((high - low) >> 1);
            if (possible(piles, mid, h)) {
                high = mid;
            } else {
                low = mid + 1;
            }
        }

        return low;
    }

    private static boolean possible(int[] piles, int speed, int h) {
        int time = 0;
        for (int x : piles) {
            // calculate number of hours needed to finish the bananas in current pile
            if (x % speed == 0) {
                time += x / speed;
            } else {
                time += 1 + x / speed; // leftover bananas in this pile will be eaten in next 1 hour
            }
        }

        return (time <= h);
    }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int totalLength = nums1.length + nums2.length;
        int toStop = (totalLength / 2) + 1;
        int l = 0;
        int r = 0;
        int[] merged = new int[totalLength];
        int idx = 0;
        while (l < nums1.length && r < nums2.length && toStop > 0) {
            if (nums1[l] <= nums2[r]) {
                merged[idx++] = nums1[l++];
            } else {
                merged[idx++] = nums2[r++];
            }
            toStop--;
        }
        while (toStop > 0 && l < nums1.length) {
            merged[idx++] = nums1[l++];
            toStop--;
        }
        while (toStop > 0 && r < nums2.length) {
            merged[idx++] = nums2[r++];
            toStop--;
        }
        if (totalLength % 2 != 0) {
            return merged[totalLength / 2];
        } else {
            return (double) (merged[totalLength / 2] + merged[(totalLength / 2) - 1]) / 2;
        }
    }
}
